package com.epam;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for SortingApp.
 */
public class SortingAppTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SortingAppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(SortingAppTest.class);
    }

    /**
     * Test when user doesn't provide any input)
     */


    public void testSortEmptyArray() {
//        int[] input = {};
//        int[] input = {5};
        String[] input = {};
//        String[] sortedArgs = {};
        SortingApp.main(input);
//        assertEquals(sortedArgs[0], input[0]);
    }

    /**
     * Test when user provides one input)
     */
    public void testSortSingleElementArray() {
//        int[] input = {5};
        String[] input = {"5"};
        String[] sortedArgs = {"5"};
        SortingApp.main(input);
        assertEquals(sortedArgs[0], input[0]);
    }


    /**
     * Test when user provides four inputs)
     */
    public void testSortMultipleElements() {
        String[] input = {"5", "2", "8", "1"};
        String[] sortedArgs = {"1", "2", "5", "8"};
        SortingApp.main(input);
        assertEquals(sortedArgs[0], input[0]);
        assertEquals(sortedArgs[1], input[1]);
        assertEquals(sortedArgs[2], input[2]);
        assertEquals(sortedArgs[3], input[3]);

    }


//    @ExtendWith(ParameterResolver.class)
//    public class SortingAppParamTest {
//
//        @ParameterizedTest
//        @MethodSource("testData")
//        public void testSortMultipleElements(String[] input, String[] sortedArgs) {
//            SortingApp.main(input);
//            for (int i = 0; i < input.length; i++) {
//                assertEquals(sortedArgs[i], input[i]);
//            }
//        }
//
//        private static Stream<Arguments> testData() {
//            return Stream.of(
//                    Arguments.of(new String[]{"5", "2", "8", "1"}, new String[]{"1", "2", "5", "8"})
//                    // Add more test cases as needed
//            );
//        }
//    }



    /**
     * Test when user doesn't provide valid inputs)
     */
    public void testSortInvalidInput() {
        int[] input = {};
        SortingApp.main(new String[]{"invalid"});
        assertEquals(0, input.length);
    }

    /**
     * Test when user provides ten inputs)
     */
    public void testSortTenElements() {
        String[] input = {"5", "2", "8", "1", "3", "6", "7", "4", "9", "0"};
        String[] sortedArgs = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        SortingApp.main(input);
        assertEquals(sortedArgs[0], input[0]);
        assertEquals(sortedArgs[1], input[1]);
        assertEquals(sortedArgs[2], input[2]);
        assertEquals(sortedArgs[3], input[3]);
        assertEquals(sortedArgs[4], input[4]);
        assertEquals(sortedArgs[5], input[5]);
        assertEquals(sortedArgs[6], input[6]);
        assertEquals(sortedArgs[7], input[7]);
        assertEquals(sortedArgs[8], input[8]);
        assertEquals(sortedArgs[9], input[9]);
    }

    /**
     * Test when user provides four inputs)
     */
    public void testSortElevenElements() {
        String[] input = {"5", "2", "8", "1", "3", "6", "7", "4", "10", "9", "0"};
        String[] sortedArgs = {"5", "2", "8", "1", "3", "6", "7", "4", "10", "9", "0"};
//        String[] sortedArgs = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        SortingApp.main(input);
        assertEquals(sortedArgs[0], input[0]);
        assertEquals(sortedArgs[1], input[1]);
        assertEquals(sortedArgs[2], input[2]);
        assertEquals(sortedArgs[3], input[3]);
        assertEquals(sortedArgs[4], input[4]);
        assertEquals(sortedArgs[5], input[5]);
        assertEquals(sortedArgs[6], input[6]);
        assertEquals(sortedArgs[7], input[7]);
        assertEquals(sortedArgs[8], input[8]);
        assertEquals(sortedArgs[9], input[9]);
        assertEquals(sortedArgs[10], input[10]);
    }


}
