package com.epam;

import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Sorting App.
 * It is a small Java application that takes up to ten command-line arguments as integer values,
 * sorts them in the ascending order, and then prints them into standard output.
 */
public class SortingApp {

    private static final Logger logger = LogManager.getLogger(SortingApp.class);

    /**
     * main method
     * @param args which will be user input into terminal
     */
    public static void main(String[] args) {
        // Ensure that there are at most 10 arguments
        if (args.length > 10) {
//            System.out.println("Error: Too many arguments. Please provide up to 10 integer values.");
            logger.error("Error: Too many arguments. Please provide up to 10 integer values.");
            return;
        }

        if (args.length == 0) {
//            System.out.println("Error: No arguments. Please provide some integer values.");
            logger.error("Error: No arguments. Please provide some integer values.");
            return;
        }

        // Parse command-line arguments as integers
        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.out.println("Error: Argument " + args[i] + " is not a valid integer.");
                return;
            }
        }

        // Sort the integers
        Arrays.sort(numbers);
        logger.info("sorting the int array finished");

        // Print the sorted values
        int index = 0;
        System.out.println("Sorted Values:");
        for (int number : numbers) {
            args[index] = Integer.toString(number);
            index++;
            System.out.println(number);
        }
    }
};
